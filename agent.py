# Grupa 244, Nita Bogdan

from world import World
from node import Node
from utils import find_state_in_arr
from typing import List, Set, Tuple, Dict, Callable, Any


class Agent:
    """
        Clasa ce creaza si parcurge arborele de 
        cautare pe baza problemei si constregerilor ei.
    """

    def __init__(self, world: World):
        super().__init__()
        self.world = world

    def solve(self, max_iter=1250):
        """
            Funtie de cautare de drum pe baza A* si 
            lumea setata in constructor
        """

        # lista nodurilor ce urmeaza sa fie vizitate,
        # initializata cu nodul radacina al lumii
        opened: List[Node] = [self.world.root]
        # lista nodurilor deja vizitate
        closed: List[Node] = []

        # flag ce ne spune daca starea finala a fost intalnita
        found: bool = False

        # contor de iteratii
        iter = 0
        # cat timp exista noduri de parcurs, le parcurgem
        while len(opened) > 0:
            # limita sintetica pentru numarul de iteratii
            if iter >= max_iter:
                print(f"Ended search on iteration #{iter}:\n")
                break
            else:
                print(f"Iteration #{iter}:\n")
                iter += 1

            # selectam primul nod din lista opened ordonata
            # crescator dupa f_value si descrescator dupa g_value
            current: Node = min(
                opened,
                key=lambda node: (+node.f_value, -node.g_value)
            )
            # il scoatem din opened si il adaugam in closed
            opened.remove(current)
            closed.append(current)

            # verificam daca starea acestuia este finala
            if self.world.is_final(current):
                found = True
                break

            # generam lista de noduri succesoare candidate
            candidates = self.world.expand(current)

            # parcurgem lista de candidati si ponderea aferenta
            child: Node
            weight: float
            for child, weight in candidates:
                # verificam ca nodul candidat sa nu se afle
                # deja in drumul de la radacina la parinte
                # astfel evitand ciclurile
                if (not current.is_in_path_to_root(child)):
                    # pentru fiecare dintre candidati calculam
                    # g_value si f_value pe baza datelor nodului
                    # parinte si ponderea drumului pana la acesta
                    child.parent = current
                    child.g_value = current.g_value + weight
                    child.f_value = child.g_value + child.h_value

                    # cautam nodul candidat in lista opened respectiv closed
                    closed_node = find_state_in_arr(closed, child)
                    opened_node = find_state_in_arr(opened, child)

                    # daca il gasim intr-una din liste cu o valoare
                    # f_value mai mare decat ce-a calculata acum
                    # il scoatem din lista respectiva
                    if closed_node is not None:
                        if (child.f_value < closed_node.f_value):
                            closed.remove(closed_node)
                    elif opened_node is not None:
                        if (child.f_value < opened_node.f_value):
                            opened.remove(opened_node)

                    # adaugam nodul succesor la lista opened
                    opened.append(child)

                    print(f'{current.state}\n{child.descripton}\n{child.state}\n')

        # daca starea finala a fost gasita returnam drumul pana la aceasta
        return current.get_path_from_root() if found else None
