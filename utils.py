# Grupa 244, Nita Bogdan

from node import Node
from vessel import Vessel
from requirement import Requirement
from typing import List


def find_state_in_arr(arr: List[Node], node: Node):
    """
        Functie ce gaseste un nod intr-o list pe baza 
        starii continute de acesta.
    """
    for i in range(len(arr)):
        if arr[i].state == node.state:
            return arr[i]
    return None


def find_best_fit(
        options: List[Vessel],
        requirement: Requirement,
        default: Vessel = Vessel()) -> Vessel:
    """
        Functie ce cauta cel mai bun vas ce respecta cerinta data.
    """
    # selectam vasele ce au aceasi culoare ca cea specificata
    # in cerinta apoi ordonam dupa diferenta absoluta a
    # cantitatii de apa din vas si cea specificata,
    # in cele din urma, selectam vasul cu diferenta minima
    return min(
        [vessel for vessel in options if vessel.color == requirement.color],
        key=lambda vessel: abs(vessel.curr_qty - requirement.qty),
        default=default
    )
