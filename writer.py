# Grupa 244, Nita Bogdan

from world import World
from node import Node
from utils import find_state_in_arr
from typing import List, Set, Tuple, Dict, Callable, Any


class Writer:
    """
        Clasa ce se ocupa cu scrierea in fisier
    """

    def __init__(self, path: str):
        super().__init__()
        self.path = path
        self.flush()
        pass

    def flush(self):
        with open(self.path, 'w') as file:
            file.truncate(0)
            pass
        pass

    def append(self, title: str, path: List[Node], duration: float):
        with open(self.path, 'a') as file:
            file.write(
                f"\n\n------------------ {title} -----------------------\n\n"
            )
            file.write(f"Timp de executie: {duration:.2f}ms\n")
            if path == None or len(path) == 0:
                file.write(
                    "Nu a fost gasit un drum de la nodul start la scop.\n"
                )
            else:
                file.write(
                    f"Drum de cost minim gasit cu lungimea {len(path)}:\n"
                )
                separator = "\n\n"
                content = separator.join([str(x) for x in path])
                file.write(content)
            pass
        pass
