# Grupa 244, Nita Bogdan

from typing import List, Tuple, Dict, Callable, Any


class Vessel:
    """
        Clasa ce contine datele despre un vas precum id-ul acestuia, 
        cantitatea maxima, cantitatea curenta, si culoarea apei.
    """
    COLOR_NONE = 'none'
    COLOR_UNDEFINED = 'undefined'

    def __init__(self, id: int = 0, max_qty: int = 0, curr_qty: int = 0, color: str = None):
        super().__init__()
        self.id = id
        self.max_qty = max_qty
        self.curr_qty = curr_qty
        self.color = color

    def pour(self, target: 'Vessel', color_table: Dict[Tuple[str, str], str]) -> int:
        """
            Realizeaza actiunea de turnarea a apei dintr-un vas in altul
            respectand cantitatea ramasa in vasul sursa si cantitatea 
            disponibila in vasul destinatie si seteaza culorile apei 
            dupa tabelul de culori pus la dispozitie.
        """
        transfer_qty = min(self.curr_qty, target.get_empty_qty())
        if transfer_qty != 0:
            self.curr_qty -= transfer_qty
            target.curr_qty += transfer_qty

            if (self.color, target.color) in color_table:
                target.color = color_table[self.color, target.color]
            elif target.is_color(Vessel.COLOR_NONE):
                target.color = self.color
            else:
                target.color = Vessel.COLOR_UNDEFINED

            if self.curr_qty == 0:
                self.color = Vessel.COLOR_NONE
        return transfer_qty
        pass

    def get_empty_qty(self) -> int:
        return self.max_qty - self.curr_qty

    def is_empty(self) -> bool:
        return self.curr_qty == 0

    def is_full(self) -> bool:
        return self.curr_qty == self.max_qty

    def is_color(self, color: str):
        return self.color == color

    def __eq__(self, value):
        if self.__class__ != value.__class__:
            return False
        return self.__dict__ == value.__dict__

    def __str__(self):
        return "{}: {} {} {}".format(
            self.id,
            self.max_qty,
            self.curr_qty,
            self.color,
        )

    def __repr__(self):
        return "{}: {} {} {}".format(
            self.id,
            self.max_qty,
            self.curr_qty,
            self.color,
        )
