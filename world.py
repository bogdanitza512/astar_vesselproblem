# Grupa 244, Nita Bogdan

from node import Node
from vessel import Vessel
from requirement import Requirement
from typing import List, Set, Tuple, Dict, Callable, Any
from copy import deepcopy
from utils import find_best_fit


class World:
    """
        Clasa ce contine datele problemei: starea initiala,
        tabelul de combinatii de culori si cerintele starii finale.
    """

    def __init__(
        self,
        color_table: Dict[Tuple[str, str], str],
        initial_state: List[Vessel],
        final_state: List[Requirement],
        h_func: Callable[[List[Vessel], List[Requirement]], float]
    ):
        super().__init__()
        self.color_table = color_table

        self.nodes: List[Node] = [
            Node(
                state=initial_state,
                desciption="Stare initiala.",
                h_value=h_func(initial_state, final_state)
            )
        ]
        self.root = self.nodes[0]
        self.goal = final_state

        self.h_func = h_func
        pass

    def expand(self, node: Node) -> Set[Tuple[Node, float]]:
        """
            Expandeaza nodul cu toate starile succesoare posibile in care se poate ajunge din acesta.
            Este considerata stare succesor posibila orice turnare valida de apa dintr-unul 
            dintre vase in altul.
        """
        result: Set[Tuple[Node, float]] = set()

        # verificam daca nodul accepta stari succesori
        if not self.is_expandable(node):
            return result

        # parcurgem lista de vase, generand succesori prin
        # turnarea fiecarui vas ce contine apa in celelalte vase
        for x in range(len(node.state)):
            # verificam ca nu incercam sa turnam dintr-un vas gol
            if node.state[x].is_empty():
                continue
            for y in range(len(node.state)):
                # verificam ca nu incercam sa turnam in acelasi vas
                if(x == y):
                    continue
                # facem o copie a stari pe care urmeaza sa o modificam
                vessels = deepcopy(node.state)
                # turnam dintr-un vas in altul respectand tabela de culori
                transfer_qty = vessels[x].pour(
                    target=vessels[y],
                    color_table=self.color_table
                )
                # adaugam noua stare in lista de stari succesori
                # impreuna cu pondera drumului pana la aceasta stare
                # adica cantitatea de apa ce a fost necesar sa o transferam
                result.add(
                    (
                        Node(
                            state=vessels,
                            desciption=self.make_description(
                                source=node.state[x].id,
                                qty=transfer_qty,
                                color=node.state[x].color,
                                destination=node.state[y].id
                            ),
                            h_value=self.h_func(vessels, self.goal),
                        ),
                        transfer_qty
                    )
                )
                pass
            pass
        return result
        pass

    def is_expandable(self, node: Node) -> bool:
        """
            Verifica daca nodul poate fi expandat. Daca vasele nu sunt toate fie goale,
            pline sau contin o culoare nedefinita inseamna ca nodul poate fi expandat.
        """
        all_empty: bool = True
        all_full: bool = True
        all_undefined: bool = True

        # parcurgem lista de vase verificand conditiile de validitate:
        # - nu trebuie sa avem toate vasele goale
        # - nu trebuie sa avem toate vasele pline
        # - nu trebuie sa avem toate vasele cu continunt
        #   de apa de culoare nedefinita
        vessel: Vessel
        for vessel in node.state:
            all_empty &= vessel.is_empty()
            all_full &= vessel.is_full()
            all_undefined &= vessel.is_color(Vessel.COLOR_UNDEFINED)

        if all_empty or all_full or all_undefined:
            print(f'Node is impossible to expand.')
            return False

        return True

    def is_final(self, node: Node) -> bool:
        """
            Verifica daca nodul respecta cerintele starii finale.
        """
        # pentru fiecare cerinta a starii finale cautam
        # cel mai "apropiat" vas ce "pare" sa o respecte
        # si verificam strict daca o respecta sau nu
        result = True
        for requirement in self.goal:
            match = requirement.find_best_fit(node.state)
            result &= requirement.qty == match.curr_qty
        return result

    def make_description(self, source: int, qty: int, color: str, destination: int):
        """
            Creaza descrierea actiunii unei turnari de apa dintr-un vas in altul
        """
        return f'Din vasul {source} s-au turnat {qty} litri de apa de culoare {color} in vasul {destination}.'

    pass
