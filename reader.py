# Grupa 244, Nita Bogdan

from vessel import Vessel
from requirement import Requirement
from enum import auto, Enum
from typing import List, Set, Dict, Tuple


class Reader:
    """
        Clasa ce se ocupa cu citirea din fisier 
    """
    class Token(Enum):
        color_table = 'tabel_culori'
        initial_state = 'stare_initiala'
        final_state = 'stare_finala'
        pass

    def __init__(self, path):
        super().__init__()
        self.path = path
        self.current = Reader.Token.color_table
        self.tokens = set([token.value for token in Reader.Token])
        self.switch = {
            Reader.Token.color_table: self.add_to_color_table,
            Reader.Token.initial_state: self.add_to_initial_state,
            Reader.Token.final_state: self.add_to_final_state,
        }

        self.vessel_counter = 0
        self.color_table: Dict[Tuple[str, str], str] = {}
        self.initial_state: List[Vessel] = []
        self.final_state: List[Requirement] = []

        self.read()

    def read(self):

        with open(self.path, 'r') as file:
            lines = file.readlines()
            for line in lines:
                words = line.split()

                intersection = self.tokens.intersection(words)
                if len(intersection) != 0:
                    value = intersection.pop()
                    self.current = Reader.Token(value)
                    continue

                func = self.switch.get(self.current, None)
                if func is not None:
                    func(words)

    def add_to_color_table(self, words):
        color_01 = words[0]
        color_02 = words[1]
        color_03 = words[2]

        self.color_table[(color_01, color_02)] = color_03
        self.color_table[color_02, color_01] = color_03

    def add_to_initial_state(self, words):
        max_qty = int(words[0])
        curr_qty = int(words[1])
        color = words[2] if curr_qty != 0 else Vessel.COLOR_NONE

        self.initial_state.append(
            Vessel(
                id=len(self.initial_state) + 1,
                max_qty=max_qty,
                curr_qty=curr_qty,
                color=color
            )
        )

        self.vessel_counter += 1

    def add_to_final_state(self, words):
        qty = int(words[0])
        color = words[1]

        self.final_state.append(
            Requirement(
                qty=qty,
                color=color
            )
        )

    def get_data(self):
        return (self.color_table, self.initial_state, self.final_state)
