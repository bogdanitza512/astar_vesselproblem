# Grupa 244, Nita Bogdan

from vessel import Vessel
from typing import List, Tuple, Dict, Callable, Any


class Node:
    """
        Clasa ce contine datele despre un nod din graful de cautare
    """

    def __init__(
        self,
        state: List[Vessel],
        desciption: str = '',
        parent: "Node" = None,
        g_value: float = 0,
        h_value: float = 0,
    ):
        super().__init__()
        self.state = state
        self.parent = parent
        self.g_value = g_value
        self.h_value = h_value
        self.descripton = desciption
        self.f_value = self.g_value + self.h_value
        pass
    pass

    def get_path_from_root(self):
        current_node = self
        path = [current_node]
        while current_node.parent is not None:
            path = [current_node.parent] + path
            current_node = current_node.parent
        return path

    def is_in_path_to_root(self, node: "Node"):
        current_node = self
        while current_node is not None:
            if node.state == current_node.state:
                return True
            current_node = current_node.parent
        return False

    def __str__(self):
        return "{}\n{}".format(
            self.descripton,
            '\n'.join([str(x) for x in self.state]),
        )

    def __repr__(self):
        return "{}\n{}".format(
            self.descripton,
            '\n'.join([str(x) for x in self.state]),
        )
