# Grupa 244, Nita Bogdan
from vessel import Vessel
from typing import List, Tuple, Dict, Callable, Any


class Requirement:
    def __init__(self, qty: int = 0, color: str = Vessel.COLOR_NONE):
        super().__init__()
        self.qty = qty
        self.color = color

    def find_best_fit(
            self,
            options: List[Vessel],
            default: Vessel = Vessel()) -> Vessel:
        """
            Functie ce cauta cel mai bun vas ce respecta cerinta data.
        """
        # selectam vasele ce au aceasi culoare ca cea specificata
        # in cerinta apoi ordonam dupa diferenta absoluta a
        # cantitatii de apa din vas si cea specificata,
        # in cele din urma, selectam vasul cu diferenta minima
        return min(
            [vessel for vessel in options if vessel.color == self.color],
            key=lambda vessel: abs(vessel.curr_qty - self.qty),
            default=default
        )
