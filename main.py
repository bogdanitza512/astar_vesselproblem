# Grupa 244, Nita Bogdan

from reader import Reader
from writer import Writer
from vessel import Vessel
from node import Node
from world import World
from agent import Agent
from typing import List
from requirement import Requirement
from utils import find_best_fit
from time import time


def h_func_manhattan(current: List[Vessel], final: List[Requirement]) -> int:
    """
        Functie euristica bazata pe distanta Manhattan.
        Avand in vedere ca ponderea dintre noduri este 
        egala cu cantitatea de apa mutata dintr-unul 
        dintre vase in altul la schimbarea de stare => 
        aceasta euristica ia valori cel mult egale 
        cu costul real al drumului.
    """
    distance = 0
    for requirement in final:
        match = requirement.find_best_fit(current)
        distance += abs(match.curr_qty - requirement.qty)

    return distance


def h_func_hamming(current: List[Vessel], final: List[Requirement]) -> int:
    """
        Functie euristica bazata pe distanta Hamming.
        Avand in vedere ca ponderea dintre noduri este 
        egala cu cantitatea de apa mutata dintr-unul 
        dintre vase in altul la schimbarea de stare 
        iar distanta Hamming precizeaza doar numarul
        campurilor in care starea curenta este diferita
        de starea finala => aceasta euristica ia valori
        mai mici decat costul real al drumului 
    """
    distance = 0
    for requirement in final:
        match = requirement.find_best_fit(current)
        distance += 1 if match.curr_qty != requirement.qty else 0

    return distance


def h_func_rambo(current: List[Vessel], final: List[Requirement]) -> int:
    distance = 0
    for requirement in final:
        curr_colors = [vessel.color for vessel in current]
        distance += 1 if requirement.color not in curr_colors else 0

    return distance


input_files = [
    'input/input_01.txt',
    'input/input_02.txt',
    'input/input_03.txt',
    'input/input_04.txt',
]

heuristics = {
    'Manhattan': h_func_manhattan,
    'Hamming': h_func_hamming,
    'Rambo': h_func_rambo,
}

if __name__ == "__main__":
    for index, input_file in enumerate(input_files):

        reader = Reader(
            path=input_file
        )
        writer = Writer(
            path=f"output/output_{(index+1):02d}.txt"
        )

        color_table, initial_state, final_state = reader.get_data()

        for name, func in heuristics.items():
            print(f'\n\nHeuristic: {name}\n\n')
            start = time()

            world = World(
                color_table=color_table,
                initial_state=initial_state,
                final_state=final_state,
                h_func=func,
            )

            a_star = Agent(world)
            path = a_star.solve()

            end = time()
            duration = (end-start) * 1000
            writer.append(
                title=f'Rezultate: {name}',
                path=path,
                duration=duration,
            )
            pass
        pass
    pass
